##
## epson_crawler.py
## sophie stephenson
## november 2020
##
## this script was created to aid in a research project regarding
## ar and vr authentication. we wanted to gather information about
## apps in the prominent ar and vr app stores and determine which
## apps were likely to require authentication.

import json
import csv

f = open('epson.json')
data = json.load(f)
f.close()

with open("epson_apps.csv", "w", newline='') as out:
    writer = csv.writer(out)
    writer.writerow(['Title', 'Category', 'Price'])
    for item in data['record']:
        print(item['appName'])
        title = item['appName']
        cat   = item['category']
        price = item['price']

        writer.writerow([title, cat, price])

