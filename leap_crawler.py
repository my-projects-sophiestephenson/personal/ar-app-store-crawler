##
## leap_crawler.py
## sophie stephenson
## november 2020
##
## this script was created to aid in a research project regarding
## ar and vr authentication. we wanted to gather information about
## apps in the prominent ar and vr app stores and determine which
## apps were likely to require authentication..

import json
import csv

f = open('leap_apps.json')
data = json.load(f)
f.close()

ids = list()

with open("leap_apps.csv", "w", newline='') as out:
    writer = csv.writer(out)
    writer.writerow(['Title', 'ID', 'Description', 'Release Date', 'Device Inputs',
                     'Category', 'Price', 'Publisher', 'Purchase token', 'Privileges',
                     'Minimum OS Version', 'SDK Version', 'Entitled', 'Wished', 'OS Compatible',
                     'Release Notes', 'Keywords', 'Content Descriptors', 'Maturity Ratings',
                     'Assets', 'Package', 'Countries'])
    
    for item in data['items']:
        print(item['name'])
        if item['id'] in ids:
            continue
        ids.append(item['id'])

        inputs = list()
        for i  in item['device_inputs']:
            inputs.append(i['name'])

        if 'sdk_version' in item.keys():
            sdk = item['sdk_version']
        else:
            sdk = "None"

        writer.writerow([item['name'], item['id'], item['short_description'], item['published_at'],
                         inputs, item['categories'], item['price'], item['publisher'], item['purchase_token'],
                         item['privileges'], item['minimum_os_version'], sdk,
                         item['entitled'], item['wished'], item['os_compatible'], item['release_notes'],
                         item['keywords'], item['content_descriptors'], item['maturity_ratings'],
                         item['assets'], item['package'], item['countries']])


