##
## holo_crawler.py
## sophie stephenson
## november 2020
##
## this script was created to aid in a research project regarding
## ar and vr authentication. we wanted to gather information about
## apps in the prominent ar and vr app stores and determine which
## apps were likely to require authentication.


from html.parser import HTMLParser
import requests
import csv

# define parser for the app store 
class AppStoreParser(HTMLParser):
    app_links = []

    # look for links to each site
    def handle_starttag(self, tag, attrs):
        if (tag == 'a'):
            for attr in attrs:
                if attr[0] == 'href' and attr[1].startswith('/en-us/') and not page_buttons in attr[1]:
                    self.app_links.append(attr[1])


# define parser for each app page
class AppParser(HTMLParser):
    data = dict()
    get_data = False
    looking_for = "nothing"

    # interested in title, description, category, price, and release date
    def handle_starttag(self, tag, attrs):
        # only p we're interested in is the product description
        if tag == 'p':
            for attr in attrs:
                if attr[0] == 'id' and attr[1] == 'product-description':
                    self.get_data = True
                    self.looking_for = "description"

        # everything else is a div
        elif tag == 'div':
            for attr in attrs:
                if attr[0] == 'id':
                    switcher = {
                        'productTitle': 'title',
                        'releaseDate-toggle-target':'release-date',
                        'category-toggle-target': 'category',
                        'ProductPrice_productPrice_PriceContainer': 'price'
                    }

                    if attr[1] in switcher.keys():
                        self.looking_for = switcher[attr[1]]

                if attr[0] == 'class' and attr[1] == 'c-content-toggle':
                    self.looking_for = 'details'
                    

        # if we found the end of a path for something, get the data in that path
        else:
            pairs = [("h1", "title"),
                     ("span", "release-date"),
                     ("a", "category"),
                     ("span", "price"),
                     ("span", "publisher"),
                     ("span", "size"),
                     ("span", "permissions"),
                     ("span", "developer"),
                     ("span", "age_rating"),
                     ("span", "installation"),
                     ("span", "language")]
            if (tag, self.looking_for) in pairs:
                self.get_data = True

            pretags = {
                'publisher-first': 'publisher',
                'language-first': 'language',
                'permissions-first': 'permissions'}

            if tag == 'span' and self.looking_for in pretags.keys():
                old = self.looking_for
                self.looking_for = pretags[old]

    # only grab data if we wanted that          
    def handle_data(self, data):
        
        if self.looking_for == 'details':
                switcher = {
                    'Published by': 'publisher-first',
                    'Approximate size': 'size',
                    'This app can': 'permissions-first',
                    'Developed by': 'developer',
                    'Age rating': 'age_rating',
                    'Installation': 'installation',
                    'Language supported': 'language-first',
                }

                if data in switcher.keys():
                    self.looking_for = switcher[data]
                    
        elif self.get_data:
            # add the data to the dictionary in the right spot
            self.data[self.looking_for] = data.strip()
            self.get_data = False
            self.looking_for = "nothing"

    def reset_fields(self):
        self.data = dict()
        self.get_data = False
        self.looking_for = "nothing"


def get_id_from_url(url):
    q_index = url.find('?')
    app_id = url[q_index - 12:q_index]
    return app_id

def set_feature(key, d):
    if key in d.keys():
        return d[key]
    return "N/A"
    
    

# ------------------------------------------------------------------------------

hololens_pages = [
'https://www.microsoft.com/en-us/store/collections/hlgettingstarted/hololens',
'https://www.microsoft.com/en-us/store/collections/hlgettingstarted/hololens?s=store&skipitems=90',
'https://www.microsoft.com/en-us/store/collections/hlgettingstarted/hololens?s=store&skipitems=180',
'https://www.microsoft.com/en-us/store/collections/hlgettingstarted/hololens?s=store&skipitems=270',
    ]

page_buttons = '/en-us/store/collections/hlgettingstarted/hololens?s=store&skipitems='

# loop through four pages in app store to get all app links
parser = AppStoreParser()

for page in hololens_pages:               
    response = requests.get(page)
    parser.feed(response.text)

app_pages = ['https://www.microsoft.com' + p for p in parser.app_links]
print(len(app_pages), "app links found")


# loop through all app links to get details about each app
app_parser = AppParser()
app_details = list()

for page in app_pages:
    # get page and raise error if necessary
    response = requests.get(page)
    response.raise_for_status()

    # save details in a list (of dictionaries) and reset parser
    app_parser.feed(response.text)

    # add id to the data
    data = app_parser.data
    data['id'] = get_id_from_url(page)
    
    app_details.append(data)
    for key in app_parser.data.keys():
        print(key + ": " + app_parser.data[key])
    print()
    
    app_parser.reset_fields()

        
# write details to csv
with open("hololens_apps.csv", "w", newline='') as out:
    writer = csv.writer(out)
    writer.writerow(['Title', 'ID', 'Description', 'Release Date', 'Category', 'Price',
                     'Publisher', 'Size', 'Permissions', 'Developer', 'Age Rating',
                     'Installation', 'Language'])
    for d in app_details:
        permissions = set_feature('permissions', d)
        dev = set_feature('developer', d)
        rating = set_feature('age_rating', d)
        installation = set_feature('installation', d)
        size = set_feature('size', d)
        language = set_feature('language', d)
        publisher = set_feature('publisher', d)
        cat = set_feature('category', d)
        
        writer.writerow([d['title'], d['id'], d['description'], d['release-date'],
                         cat, d['price'], publisher, size,
                         permissions, dev, rating, installation, language])


    
    

    
