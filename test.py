from html.parser import HTMLParser
import requests

# define parser for each app page
class AppParser(HTMLParser):
    data = dict()
    get_data = False
    looking_for = "nothing"

    # interested in title, description, category, price, and release date
    def handle_starttag(self, tag, attrs):
        # only p we're interested in is the product description
        if tag == 'p':
            for attr in attrs:
                if attr[0] == 'id' and attr[1] == 'product-description':
                    self.get_data = True
                    self.looking_for = "description"

        # everything else is a div
        elif tag == 'div':
            for attr in attrs:
                if attr[0] == 'id':
                    switcher = {
                        'productTitle': 'title',
                        'releaseDate-toggle-target':'release-date',
                        'category-toggle-target': 'category',
                        'ProductPrice_productPrice_PriceContainer': 'price'
                    }

                    if attr[1] in switcher.keys():
                        self.looking_for = switcher[attr[1]]

                if attr[0] == 'class' and attr[1] == 'c-content-toggle':
                    self.looking_for = 'details'
                    

        # if we found the end of a path for something, get the data in that path
        else:
            pairs = [("h1", "title"),
                     ("span", "release-date"),
                     ("a", "category"),
                     ("span", "price"),
                     ("span", "publisher"),
                     ("span", "size"),
                     ("span", "permissions"),
                     ("span", "developer"),
                     ("span", "age_rating"),
                     ("span", "installation"),
                     ("span", "language")]
            if (tag, self.looking_for) in pairs:
                self.get_data = True

            pretags = {
                'publisher-first': 'publisher',
                'language-first': 'language',
                'permissions-first': 'permissions'}

            if tag == 'span' and self.looking_for in pretags.keys():
                old = self.looking_for
                self.looking_for = pretags[old]

    # only grab data if we wanted that          
    def handle_data(self, data):
        
        if self.looking_for == 'details':
                switcher = {
                    'Published by': 'publisher-first',
                    'Approximate size': 'size',
                    'This app can': 'permissions-first',
                    'Developed by': 'developer',
                    'Age rating': 'age_rating',
                    'Installation': 'installation',
                    'Language supported': 'language-first',
                }

                if data in switcher.keys():
                    self.looking_for = switcher[data]
                    
        elif self.get_data:
            # add the data to the dictionary in the right spot
            self.data[self.looking_for] = data.strip()
            self.get_data = False
            self.looking_for = "nothing"

    def reset_fields(self):
        self.data = dict()
        self.get_data = False
        self.looking_for = "nothing"




app_parser = AppParser()

page = 'https://www.microsoft.com/en-us/p/solobirthday/9pdvqbb6ldl7?cid=msft_web_collection&activetab=pivot:overviewtab'


# get page and raise error if necessary
response = requests.get(page)
response.raise_for_status()

# save details in a list (of dictionaries) and reset parser
app_parser.feed(response.text)


for key in app_parser.data.keys():
    print(key + ": " + app_parser.data[key])
